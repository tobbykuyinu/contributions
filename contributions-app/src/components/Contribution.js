import React  from 'react';
import gql from 'graphql-tag';
import { propType } from 'graphql-anywhere';

function Contribution(
    {
        data: { txid, address, currency, value, createdAt },
    },
) {
    return (
        <div className="list-item">
            <table>
                <tr>
                    <th>TXID</th><th>Address</th><th>Currency</th><th>Value</th><th>Created At</th>
                </tr>
                <th>
                    <td>{txid}</td><td>{address}</td><td>{currency}</td><td>{value}</td><td>{createdAt}</td>
                </th>
            </table>
        </div>
    );
}

Contribution.fragments = {
    contribution: (
        gql`
            fragment ContributionFragment on Contribution {
                txid
                address
                currency
                value
                createdAt
            }
        `
    ),
};

Contribution.propTypes = {
    data: propType(Contribution.fragments.contribution).isRequired
};

export default Contribution;
