export const BTCFilter = () => {
    return {
        type: 'FILTER',
        currency: 'BTC'
    }
};

export const LTCFilter = () => {
    return {
        type: 'FILTER',
        currency: 'LTC'
    }
};

export const ETHFilter = () => {
    return {
        type: 'FILTER',
        currency: 'ETH'
    }
};

