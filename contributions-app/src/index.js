import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import ApolloClient  from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter } from 'react-router-dom';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

import reducer from './reducers';
import App from './pages/App';
import './index.css';

const { REACT_APP_API_HOST = 'http://localhost:3000' } = process.env;
const apolloCache = new InMemoryCache(window.__APOLLO_STATE__);
const store = createStore(reducer);

const client = new ApolloClient({
    cache: apolloCache,
    link: new HttpLink({ uri: `${REACT_APP_API_HOST}/graphql` })
});

function render() {
    ReactDOM.render((
        <ApolloProvider client={client}>
            <BrowserRouter>
                <App store={store}/>
            </BrowserRouter>
        </ApolloProvider>
    ), document.getElementById('root'));
}

store.subscribe(render);
render();
