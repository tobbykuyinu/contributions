export default `enum Currency {
    BTC
    ETH
    LTC
}

type Contribution {
    txid: ID!
    address: String!
    currency: Currency!
    value: Int!
    createdAt: Float!
}

type Query {
    contributions(currency: Currency): [Contribution]
}

schema {
    query: Query
}`;
