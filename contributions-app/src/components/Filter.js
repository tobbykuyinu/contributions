import React  from 'react';
import gql from 'graphql-tag';
import { propType } from 'graphql-anywhere';
import { PropTypes } from 'prop-types';

const alignStyles = {
    fontSize: '14px',
    lineHeight: '1.5rem',
    padding: '1em 1em 1em 1em',
};
const currencies = ['BTC', 'ETH', 'LTC'];

function Filter(
    {
        filters: { currency },
        onUpdateFilter
    },
) {
    return (
        <div className="list-item filter" style={alignStyles}>
            {currencies.map((curr) =>
                <label className="checkbox" key={curr}>
                    <input
                        type="checkbox"
                        checked={currency.indexOf(curr.toLowerCase()) >= 0}
                        name={curr}
                        value={curr}
                        onChange={event => onUpdateFilter(curr.toLowerCase())}
                    />
                    <span className="checkbox-custom">{curr}</span>
                </label>
            )}
        </div>
    );
}

Filter.fragments = {
    filter: (
        gql`
            fragment FilterFragment on Contribution {
                currency
            }`
    ),
};

Filter.propTypes = {
    filters: propType(Filter.fragments.filter).isRequired,
    onUpdateFilter: PropTypes.func,
};

export default Filter;
