import DataLoader from 'dataloader';
import findByIds from 'mongo-find-by-ids';

export default class Contribution {
  constructor(context) {
    this.context = context;
    this.collection = context.db.collection('contribution');
    this.pubsub = context.pubsub;
    this.loader = new DataLoader(ids => findByIds(this.collection, ids));
  }

  findOneById(id) {
    return this.loader.load(id);
  }

  all({ currency, lastCreatedAt = 0, limit = 10 }) {
    const params = { createdAt: { $gt: lastCreatedAt } };

    if (currency) {
      params.currency = currency;
    }

    return this.collection.find(params).sort({ createdAt: 1 }).limit(limit).toArray();
  }

  async insert(doc) {
    const docToInsert = Object.assign({}, doc, {
      createdAt: Date.now(),
      updatedAt: Date.now(),
    });
    const id = (await this.collection.insertOne(docToInsert)).insertedId;
    this.pubsub.publish('contributionInserted', await this.findOneById(id));
    return id;
  }

  async insertMock(doc, createdAt) {
    const docToInsert = Object.assign({}, doc, {
      createdAt: createdAt,
      updatedAt: createdAt,
    });
    const id = (await this.collection.insertOne(docToInsert)).insertedId;
    this.pubsub.publish('contributionInserted', await this.findOneById(id));
    return id;
  }

  async updateById(id, doc) {
    const ret = await this.collection.update({ _id: id }, {
      $set: Object.assign({}, doc, {
        updatedAt: Date.now(),
      }),
    });
    this.loader.clear(id);
    this.pubsub.publish('contributionUpdated', await this.findOneById(id));
    return ret;
  }

  async removeById(id) {
    const ret = this.collection.remove({ _id: id });
    this.loader.clear(id);
    this.pubsub.publish('contributionRemoved', id);
    return ret;
  }
}
