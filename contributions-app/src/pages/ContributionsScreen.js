import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import ChartData from '../components/ChartData';
import ContributionList from '../components/ContributionList';

export default (store) => {
    const contribData = graphql(
        gql`
        query ContributionsQuery {
            btc: contributions(currency: BTC, limit: 100) {
                ...ContributionListContributionFragment
            }
            eth: contributions(currency: ETH, limit: 100) {
                ...ContributionListContributionFragment
            }
            ltc: contributions(currency: LTC, limit: 100) {
                ...ContributionListContributionFragment
            }
        }
        ${ContributionList.fragments.contribution}
    `,
        {
            options: {
                forceFetch: true,
                pollInterval: 10 * 1000,
            },
            props({ data: { loading, error, btc, eth, ltc } }) {
                if (loading) {
                    return { loading, store };
                }
                if (error) {
                    return { error, store };
                }

                btc = btc || [];
                eth = eth || [];
                ltc = ltc || [];
                return { btc, eth, ltc, store };
            },
        },
    );

    return contribData(ChartData);
}
