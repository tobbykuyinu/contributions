const resolvers = {
  Contribution: {
    id(contribution) {
      return contribution._id;
    },
  },
  Query: {
    contributions(root, { lastCreatedAt, limit, currency }, { Contribution }) {
      return Contribution.all({ lastCreatedAt, limit, currency });
    },

    contribution(root, { id }, { Contribution }) {
      return Contribution.findOneById(id);
    },
  },
  Mutation: {
    async createContribution(root, { input }, { Contribution }) {
      const id = await Contribution.insert(input);
      return Contribution.findOneById(id);
    },

    async updateContribution(root, { id, input }, { Contribution }) {
      await Contribution.updateById(id, input);
      return Contribution.findOneById(id);
    },

    removeContribution(root, { id }, { Contribution }) {
      return Contribution.removeById(id);
    },
  },
  Subscription: {
    contributionCreated: contribution => contribution,
    contributionUpdated: contribution => contribution,
    contributionRemoved: id => id,
  },
};

export default resolvers;
