import React, {Fragment} from 'react';
import {propType} from 'graphql-anywhere';
import {PropTypes} from 'prop-types';
import moment from 'moment';
import Contribution from './Contribution';
import ContribChart from './Chart';
import { BTCFilter, ETHFilter, LTCFilter } from '../actions';

const ChartData = ({loading, error, store, btc = [], ltc = [], eth = []}) => {
    const totals = {};
    const contributionData = { btc, ltc, eth };
    const data = { eth: {}, btc: {}, ltc: {} };
    const contributions = [];
    const proportions = [];
    const filters = { btc: BTCFilter, eth: ETHFilter, ltc: LTCFilter };

    Object.keys(store.getState()).forEach((currency) => {
        if (store.getState()[currency]) {
            contributionData[currency].forEach((contrib) => {
                const key = moment(contrib.createdAt).format('YYYY-MM-DD');
                data[currency][key] = (data[currency][key] || 0) + contrib.value;
                totals[currency] = (totals[currency] || 0) + contrib.value;
            });

            const curr = currency.toUpperCase();
            contributions.push({ name: curr, data: data[currency]});
            proportions.push([curr, totals[currency] || 0]);
        }
    });

    const toggleCurrency = (currency) => {
        store.dispatch(filters[currency]());
    };

    const title = 'Contributions';

    return (<Fragment>
        {loading && <div>Loading...</div>}
        {error && <div>{error.toString()}</div>}
        {!loading && !error &&
        <ContribChart title={title} updateFilter={toggleCurrency} store={store} data={{ contributions, proportions }}/>}
    </Fragment>)
};

ChartData.propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.object,
    store: PropTypes.object,
    btc: PropTypes.arrayOf(propType(Contribution.fragments.contribution)),
    ltc: PropTypes.arrayOf(propType(Contribution.fragments.contribution)),
    eth: PropTypes.arrayOf(propType(Contribution.fragments.contribution)),
};

export default ChartData;
