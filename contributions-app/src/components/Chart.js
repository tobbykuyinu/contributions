import React, {Fragment} from 'react';
import {PropTypes} from 'prop-types';
import ReactChartkick, {LineChart, PieChart} from 'react-chartkick'
import Chart from 'chart.js'
import Filter from './Filter';

ReactChartkick.addAdapter(Chart);

const ContribChart = ({title, updateFilter, store, data}) => {
    const currencies = Object.keys(store.getState()).filter((curr) => store.getState()[curr]);
    return (<Fragment>
        <div>
            <div className="page lists-show">
                <nav>
                    <h3 className="js-edit-list title-page" style={{textAlign: 'center'}}>
                        <span className="title-wrapper">{title}</span>
                    </h3>
                </nav>
            </div>
            <div className="list-items content-scrollable" style={{margin: 40}}>
                <Filter filters={{currency: currencies}} onUpdateFilter={updateFilter}/>
                <LineChart className="line-chart" data={data.contributions} download={true}/>
                <PieChart className="pie-chart" data={data.proportions}/>
            </div>
        </div>}
    </Fragment>)
};

ContribChart.propTypes = {
    title: PropTypes.string,
    updateFilter: PropTypes.func,
    store: PropTypes.object,
    data: PropTypes.object
};

export default ContribChart;
