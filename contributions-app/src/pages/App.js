import React  from 'react';
import { Route } from 'react-router-dom';

import ContributionsScreen from './ContributionsScreen';

const App = ({ store }) => (
    <div id="container" className="menuOpen">
        <div id="content-container">
            <Route exact path="/" component={ContributionsScreen(store)} />
        </div>
    </div>
);

export default App;