const iniState = { btc: true, ltc: true, eth: true };

export default (state = iniState, action) => {
    if (action.type === 'FILTER') {
        return Object.assign({}, state, {
            [action.currency.toLowerCase()]: !state[action.currency.toLowerCase()]
        });
    }

    return state;
}
