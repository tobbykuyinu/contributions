The guide herein describes the project done and how to get it up and running.

## Project details
The context for the project is a React app and a GraphQL server which presents contributions visually on the app using
data from the server. The systems were bootstrapped respectively using different tools. There is also a base Jenkinsfile
which requires minimal customisation to work out of the box with a jenkins server

### Frontend
The frontend was setup using React App. It communicates with a GraphQL server using Apollo GraphQL. It's been built to
serve as a presentation layer for the contributions data. This was implemented as Charts (one line chart and one pie chart)
While the charts are minimalistic, there's a filter functionality. There are also a few tests for the main component
implementation with mocks in place. This can be accessed by running (within the docker container):
```cd contributions-app; yarn test```.

### Server
The server was bootstrapped using a tool called 'create-graphql-server' (https://www.npmjs.com/package/create-graphql-server).
This tool sets up necessary scaffolding for a graphql server along with mongodb. A mock data population script was added to
populate the database with sample data (though there's a relevant mutation endpoint) to make development easier.

### Relevant information and assumptions
While there are clearly loads of rooms for improvement, the product has been built minimalistic and under time constraint to
portray the necessary interaction in getting and presenting relevant data. It is in no way a production ready or production
grade system. A few things would necessarily need to be put in place before any practical use, such as, use of environment
variables (or .env files) rather than constant values in the code, a proper process manager (e.g. pm2), having static build
file output on the React project, tests on both apps, a build process, an improved docker setup, a different choice of database
along with migrations perhaps and removal of the mock process, etc. This has been built with the mentality of a demo and a simple
data fetch and presentation usecase. It has however been built in a way that should be relatively easy to modify, extend and/or
implement features on top of it (on both app and server).

## Setup Instructions
There's a simple docker setup for initializing the project. This has been put behind a simple script and can be executed
via the following line on a terminal from within the project directory:

```bash
sh start.sh
```

Upon completion of all necessary setup (and this might take a while), after the script is done executing you can monitor 
the status of the server and app startup process by running the commands: 

```bash
docker logs -f contribution_app
```
or
```bash
docker logs -f contribution_server
```

You can also ssh into the container instance with the command:
```bash
docker exec -it {container_name(contribution_app | contribution_server)} bash
```
e.g.
```bash
docker exec -it contribution_app bash
```

Once the logs suggest the app is up and running (about 1 minute from script completion), you may access the app via a
browser url on http://localhost:4000 and the graphql interactive navigator via http://localhost:3000/graphiql