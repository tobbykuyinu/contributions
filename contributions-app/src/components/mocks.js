import faker from 'faker';
import moment from 'moment';

export function buildContribution(attributes) {
    return {
        txid: faker.random.uuid(),
        address: faker.random.uuid(),
        currency: faker.random.arrayElement(['BTC', 'ETH', 'LTC']),
        value: parseInt(faker.random.number(), 10),
        createdAt: moment(faker.date.between('2018-06-01', '2018-06-30')).format('YYYY-MM-DD'),
        ...attributes,
    };
}

export function getMocks() {
    const mockSource = { btc: [], eth: [], ltc: [], all: [] };
    ['ETH', 'BTC', 'LTC'].forEach((curr) => {
        for (let i = 0; i < 30; i+=1) {
            const j = `0${i + 1}`.slice(-2);
            mockSource[curr.toLowerCase()].push(buildContribution({currency: curr, createdAt: moment(`2018-06-${j}`).toDate().getTime()}));
        }
    });

    return mockSource;
}
