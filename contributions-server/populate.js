'use strict';

import Contribution from './model/Contribution';
import faker from 'faker';
import moment from 'moment';

const buildContribution = (attributes) => {
    return {
        txid: faker.random.uuid(),
        address: faker.random.uuid(),
        currency: faker.random.arrayElement(['BTC', 'ETH', 'LTC']),
        value: parseInt(faker.random.number(), 10),
        createdAt: moment(faker.date.between('2018-06-01', '2018-06-30')).format('YYYY-MM-DD'),
        ...attributes,
    };
};

export default function populateMockData(context) {
    const contribution = new Contribution(context);
    const promises = [];

    return contribution.all({ limit: 300 }).then((contributions) => {
        contributions.forEach((contrib) => {
            promises.push(contribution.removeById(contrib._id));
        });

        ['ETH', 'BTC', 'LTC'].forEach((curr) => {
            for (let i = 0; i < 60; i += 1) {
                const contrib = buildContribution({ currency: curr });
                promises.push(contribution.insertMock(contrib, moment().subtract(i % 30, 'days').toDate().getTime()));
            }
        });

        return Promise.all(promises);
    });


}
