import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChartData from './ChartData';
import { getMocks } from "./mocks";

configure({ adapter: new Adapter() });
jest.mock('react-chartkick', () => ({
    addAdapter: () => null,
    LineChart: () => null,
    PieChart: () => null
}));

describe('ChartData', () => {
    let props;
    let mountedChart;
    const state = { btc: true, ltc: true, eth: true };
    const store = {
        dispatch: (filter) => { state[filter.currency] = !state[filter.currency] },
        getState: () => state,
    };

    const chartData = () => {
        if (!mountedChart) {
            mountedChart = mount(<ChartData {...props} />);
        }

        return mountedChart;
    };

    beforeEach(() => {
        props = {
            btc: getMocks().btc,
            ltc: getMocks().ltc,
            eth: getMocks().eth,
            store: store
        };
        mountedChart = undefined;
    });

    it('always renders filters', () => {
        const filter = chartData().find('.filter');
        expect(filter.length).toBeGreaterThan(0);
    });

    it('always renders line chart', () => {
        const filter = chartData().find('.line-chart');
        expect(filter.length).toBeGreaterThan(0);
    });

    it('always renders pie chart', () => {
        const filter = chartData().find('.pie-chart');
        expect(filter.length).toBeGreaterThan(0);
    });
});
