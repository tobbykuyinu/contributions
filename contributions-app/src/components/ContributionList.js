import React  from 'react';
import { propType } from 'graphql-anywhere';
import gql from 'graphql-tag';
import { PropTypes } from 'prop-types';

import Contribution from './Contribution';

function ContributionList(
    { contributions },
) {
    return (
        <div className="list-items">
            {contributions.map(contrib => <span>{contrib.value}</span>)}
        </div>
    );
}

ContributionList.fragments = {
    contribution: (
        gql`
            fragment ContributionListContributionFragment on Contribution {
                ...ContributionFragment
            }
            ${Contribution.fragments.contribution}
        `
    ),
};

ContributionList.propTypes = {
    contributions: PropTypes.arrayOf(propType(Contribution.fragments.contribution)).isRequired,
};

export default ContributionList;