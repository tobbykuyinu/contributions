#!/bin/bash

SERVER_IMAGE_NAME="contribution_server"
ROOT="$(pwd)"
SERVER_ROOT="${ROOT}/contributions-server"
APP_ROOT="${ROOT}/contributions-app"

docker rm -f ${SERVER_IMAGE_NAME} || true;

# First check if our server image has been built. If not, build it.
if [[ $(docker inspect --format='{{.RepoTags}}' ${SERVER_IMAGE_NAME}) == "[${SERVER_IMAGE_NAME}:latest]" ]]; then
    echo " ----- Web Server Image Available for Use. -----"
else
    echo " ----- Web Server Image Does Not Exist. Building Now. -----"
    cd ${SERVER_ROOT}
    docker build -t ${SERVER_IMAGE_NAME} ${SERVER_ROOT}
fi

# Install package files on local machine
docker run -i -v ${SERVER_ROOT}:/src ${SERVER_IMAGE_NAME} yarn

APP_IMAGE_NAME="contribution_app"

docker rm -f ${APP_IMAGE_NAME} || true;

# First check if our app image has been built. If not, build it.
if [[ $(docker inspect --format='{{.RepoTags}}' ${APP_IMAGE_NAME}) == "[${APP_IMAGE_NAME}:latest]" ]]; then
    echo " ----- Web App Image Available for Use. -----"
else
    echo " ----- Web App Image Does Not Exist. Building Now. -----"
    cd ${APP_ROOT}
    docker build -t ${APP_IMAGE_NAME} ${APP_ROOT}
fi

docker run -i -v ${APP_ROOT}:/src ${APP_IMAGE_NAME} yarn

cd ${ROOT}
docker-compose up -d
